package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
	"time"
)

var (
	histogram = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Namespace: "httpserver",
			Name:      "execution_latency_seconds",
			Help:      "This is execution latency seconds",
			Buckets:   prometheus.ExponentialBuckets(0.001, 2, 15),
		}, []string{"step"})
)

func Register() {
	err := prometheus.Register(histogram)
	if err != nil {
		panic(err)
	}
}

func NewTimer() *ExecutionTimer {
	return NewExecutionTimer(histogram)
}

func NewExecutionTimer(h *prometheus.HistogramVec) *ExecutionTimer {
	now := time.Now()
	return &ExecutionTimer{
		histogram: h,
		start:     now,
		last:      now,
	}
}

func (t *ExecutionTimer) ObserveTotal() {
	(*t.histogram).WithLabelValues("total").Observe(time.Now().Sub(t.start).Seconds())
}

type ExecutionTimer struct {
	histogram *prometheus.HistogramVec
	start     time.Time
	last      time.Time
}
