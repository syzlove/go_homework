module github.com/go_module_two

go 1.16

require (
	github.com/golang/glog v1.0.0
	github.com/prometheus/client_golang v1.11.0
	github.com/thinkeridea/go-extend v1.3.2
	gopkg.in/yaml.v2 v2.4.0
)
