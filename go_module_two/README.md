# docker 
## build
`docker build -t  selina5288/httpserver:6.0 .`
## push
`docker push selina5288/httpserver:6.0`
## run
`docker run -d --name httpserver -p 80:80 selina5288/httpserver:6.0`
## deploy k8s
以下是将httpserver部署到k8s

```bash
openssl req -x509 -nodes -days 3650 -newkey rsa:2048 -keyout tls.key -out tls.crt -subj "/CN=httpserver.com/O=httpserver"
kubectl create secret tls httpserver-tls --key tls.key --cert tls.crt

 kubectl create -f k8s/conf.yaml -n sunbaoqing
 kubectl create -f k8s/deploy.yaml -n sunbaoqing
 kubectl create -f k8s/ingress.yaml -n sunbaoqing
 kubectl create -f k8s/service.yaml -n sunbaoqing

```
 curl -H "Host: httpserver.com" https://192.168.34.2 -v -k
 
# prometheus
```bash
cd prometheus
kubectl ns monitoring
kubectl create -f setup -f grafana -f prometheus
kubectl create secret generic additional-configs --from-file=additional/prometheus-additional.yaml -n monitoring
```
## pql
`histogram_quantile(0.95, sum(rate(httpserver_execution_latency_seconds_bucket{}[5m])) by (le))`
`rate(httpserver_execution_latency_seconds_count[1m])`

